import React from 'react';
import { action } from '@storybook/addon-actions';
import { host } from 'storybook-host';
import { storiesOf } from '@storybook/react';
import { withKnobs, color, text, number } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

export default ({ title, story, contents }) => {
  const stories = storiesOf('Listing Cards', module)
    .addDecorator(host({
       align: 'center middle',
       height: '80%',
       title,
       width: 450,
     }));

  stories.add(story, withInfo({
    header: false,
    inline: true,
    source: true,
    // other possible options see in Global options section below
  })(() => contents));
  //stories.add(story, () => contents);
};
