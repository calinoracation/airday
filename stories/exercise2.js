import React from 'react';

export default (
  <div className="card-container">
    <a href="https://airbnb.com" className="listing-card">
      <img src="https://images.unsplash.com/photo-1463008420065-8274332e2be8?auto=format&fit=crop&w=250&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D" />
      <div className="description">
        <span className="price">$250</span> <span>Sphynx cat cafe</span>
      </div>
    </a>
  </div>
);
