import React from 'react';
import styled from 'styled-components';
import { action } from '@storybook/addon-actions';
import { host } from 'storybook-host';
import { storiesOf } from '@storybook/react';
import { withKnobs, color, select, text, number } from '@storybook/addon-knobs';

const CardContainer = styled.div`
  display: inline-block;
  font-family: Helvetica Neue;
  padding: 10px;
  width: 250px;
`;

const CardImage = styled.img.attrs({
  src: props => props.src,
})`
  height: 175px;
  width: 100%;
  border-radius: ${props => props.radius}px;
  overflow: hidden;
`;

const Kicker = styled.div`
  color: ${props => props.textColor};
  font-size: 12px;
  font-weight: 700;
  text-transform: uppercase;
  margin-bottom: 2px;
  line-height: 16px;
`;

const CardDescription = styled.div`
  color: ${props => props.color};
  font-weight: 700;
  font-size: ${props => props.fontSize}px;
  letter-spacing: -0.2px;
  line-height: 24px;
  max-height: 48px;
  text-overflow: ellipsis;
  -webkit-line-clamp: 2;
`;

const CardPrice = styled.div`
  font-weight: 300;
  font-size: 15px;
  letter-spacing: 0.5px;
  line-height: 24px;
  margin-top: 3px;
  text-transform: uppercase;
`;

const stories = storiesOf('Example Cards', module)
  .addDecorator(withKnobs)
  .addDecorator(host({
     align: 'center middle',
     height: '80%',
     title: 'Try the controls below to experiment',
     width: 265,
   }));

const imageChoices = {
  'http://i.imgur.com/NaY8YtK.jpg': 'Surprised Lynx',
  'http://wallpaperart.altervista.org/Immagini/gattino-erba-1024x768.jpg': 'Yawning Lynx',
  'http://i.imgur.com/GeX60cH.png': 'Angry Lynx',
};

stories.add('Lynx Card', () => (
  <CardContainer>
    <CardImage
      radius={number('Image: border-radius', 3)}
      src={select('Image: src', imageChoices, 'http://i.imgur.com/NaY8YtK.jpg')}
    />
    <Kicker textColor={color('Title: color', '#EE5804')}>
      {text('Title', 'Baby Lynx')}
    </Kicker>
    <CardDescription
      color={color('Description: color', '#484848')}
      fontSize={number('Title: font-size', 22)}
    >
      {text('Description', 'Baby Lynx Cat Cafe')}
    </CardDescription>
    <CardPrice>
      {text('Price', '$Priceless')}
    </CardPrice>
  </CardContainer>
));
