import React from 'react';
import { storiesOf } from '@storybook/react';

import './listingCard';
import '../global.css';
import './exercise2.css';

import addStory from '../addStory';
import exercise1 from './exercise1';
import exercise2 from './exercise2';

addStory({
  title: 'Create the markup',
  story: 'Unstyled',
  contents: exercise1,
});

addStory({
  title: 'Style the markup',
  story: 'Styled',
  contents: exercise2,
});
