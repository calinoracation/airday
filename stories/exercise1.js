import React from 'react';

export default (
  <a href="https://airbnb.com">
    <img src="https://images.unsplash.com/photo-1463008420065-8274332e2be8?auto=format&fit=crop&w=450&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D" />
    <div>
      <span>$4,800</span>
      <span>Hairless cats are the best</span>
    </div>
  </a>
);
